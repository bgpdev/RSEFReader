# README

## Introduction
The RSEFReader project contains a RSEFReader class written in Java that can read RSEF listings from an InputStream. For more information about RSEF see the [documentation according to APNIC.](https://www.apnic.net/about-apnic/corporate-documents/documents/resource-guidelines/rir-statistics-exchange-format/)

## Example usage:
Manually reading from an RSEF Listing:

```
// Create an RSEFReader object and read entries.
InputStream stream = ...
RSEFReader reader = new RSEFReader(stream);

while(!reader.isEOF())
{
    RSEFObject object = reader.read();

    if(object instanceof RSEFVersion)
        ...
    if(object instanceof RSEFSummary)
        ...
    if(object instanceof RSEFRecord)
        ...
}
```

Fetching RSEF Listings from the major Regional Internet Registries.
```
// Download RSEF Listings of all major Routing Registries
RSEF.download(new Date());

// Fetch all entries of the RSEF Listings of a specific date.
List<RSEFRecord> records = RSEF.load(new Date());
```