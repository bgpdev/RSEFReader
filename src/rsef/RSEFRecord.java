package rsef;

/**
 * RSEFRecord is a data record that represents an asn, ipv4 prefix or ipv6 prefix.
 */
public class RSEFRecord extends RSEFObject
{
    /** The registry that this record belongs to. */
    public String registry;

    /** ISO 3166 2-letter code of the organization to which the allocation or assignment was made. */
    public String organization;

    /** Type of Internet number resource represented in this record. Either asn, ipv4 or ipv6 */
    public String type;

    /** For ipv4 or ipv6, the base of the IP prefix. For asn the ASN number. */
    public String start;

    /** For ipv4 the amount of hosts in this prefix. For ipv6 the CIDR prefix. For asn the amount of ASN numbers. */
    public int value;

    /** The date on which this allocation was made in YYYYMMDD format. */
    public String date;

    /** Type of allocation from the set. */
    public String status;

    /** The ID handle of this object. Often a reference to an organisation (which is also related to an AS) */
    public String id;

    @Override
    public void print()
    {
        System.out.println("RIR Specification Exchange Format Record");
        System.out.println("Registry: " + registry);
        System.out.println("Organisation: " + organization);
        System.out.println("Type: " + type);
        System.out.println("Start: " + start);
        System.out.println("Value: " + value);
        System.out.println("Allocation Date: " + date);
        System.out.println("Allocation Status: " + status);

        if(id != null)
            System.out.println("Opaque ID: " + id);
    }
}
