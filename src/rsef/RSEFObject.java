package rsef;

/**
 * The base class of all RSEF object types.
 */
public abstract class RSEFObject
{
    /**
     * Prints out the contents of an RSEF entry.
     */
    public abstract void print();
}
