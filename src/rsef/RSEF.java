package rsef;

import com.google.common.io.ByteStreams;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.UnknownHostException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * RSEF contains static classes that aid in downloading and loading RSEF entries.
 */
public class RSEF
{
    /** The RSEF listings of each Regional Internet Registry. */
    private static String[][] listings = new String[][]{
            {"APNIC", "http://ftp.apnic.net/pub/stats/apnic/delegated-apnic-extended-latest"},
            {"LACNIC", "http://ftp.lacnic.net/pub/stats/lacnic/delegated-lacnic-extended-latest"},
            {"RIPE", "https://ftp.ripe.net/pub/stats/ripencc/delegated-ripencc-extended-latest"},
            {"ARIN", "http://ftp.arin.net/pub/stats/arin/delegated-arin-extended-latest"},
            {"AFRINIC", "http://ftp.afrinic.net/pub/stats/afrinic/delegated-afrinic-extended-latest"}};

    /** Rewrite resource ID's of suborganizations. */
    public final static boolean KNOWN_SIBLING_REWRITE = true;

    /**
     * Downloads the RSEFListings from a specific date.
     * @param time The time for which the RSEF Listings should be retrieved.
     * @throws Exception Any exceptions that occur during downloading.
     */
    public static void download(Instant time) throws Exception
    {
        if(!time.equals(Instant.now().truncatedTo(ChronoUnit.DAYS)))
            throw new Exception("[RSEF] Downloading Historic RSEF versions is not yet supported.");

        for(String[] listing : listings)
        {
            // Open up the file to store the archive at.
            // TODO: Should be customizable.
            File file = new File("archives/" + DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneOffset.UTC).format(time) + "/rsef/raw/" + listing[0] + ".rsef.gz");

            if (!file.exists())
            {
                // Create a new file if it does not already exist.
                file.getParentFile().mkdirs();
                file.createNewFile();

                // Construct an RSEFReader to read RSEFObjects from the data source.
                System.out.println("[" + new Date().toString() + "] Downloading: " + listing[1]);

                // Save the file to disk.
                try
                {
                    OutputStream output = new GZIPOutputStream(new FileOutputStream(file));
                    ByteStreams.copy(new URL(listing[1]).openConnection().getInputStream(), output);
                    output.close();
                }
                catch(Exception e)
                {
                    e.printStackTrace();

                    // Delete the file so we can try again on subsequent executions.
                    file.delete();

                    // Throw a new exception causing the system to reboot.
                    throw new Exception("Failed to download the latest RSEF listing.");
                }
            }
        }
    }

    public static List<RSEFRecord> getRecords(Instant time) throws Exception
    {
        List<RSEFRecord> records = new ArrayList<>();

        for(String[] listing : listings)
        {
            // Open up the file to store the archive at.
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneOffset.UTC);
            File raw = new File("archives/" + formatter.format(time) + "/rsef/raw/" + listing[0] + ".rsef.gz");

            // If the archive does not exist, try to download it.
            if (!raw.exists())
                download(time);

            // Read from the RSEF formatted archive and add entries to the list of records.
            RSEFReader reader = new RSEFReader(new GZIPInputStream(new FileInputStream(raw)));
            while (!reader.isEOF())
            {
                RSEFObject x = reader.read();
                if (x instanceof RSEFRecord)
                {
                    RSEFRecord record = (RSEFRecord) x;
                    if(KNOWN_SIBLING_REWRITE && record.id != null)
                        record.id = rewrite(record.id);

                    records.add(record);
                }
            }
        }

        return records;
    }

    /**
     * Rewrites organizational ID's of matching (sub)-organizations
     * @param id The ID that may be rewritten into something else
     * @return The (potentially) rewritten ID, might be the same as the input.
     */
    private static String rewrite(String id)
    {
        switch (id)
        {
        /*
         * The USAISC does not exist anymore, it falls under the US Department of Defence DoD.
         **/
            case "c096bf755fee3dfb7b9046461595ebd0":
                return "bb474b75b6f23182ffa56daf1cf9ec23";
            default:
                return id;
        }
    }
}
