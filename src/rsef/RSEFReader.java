package rsef;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * RIR Statistics Exchange Format Reader
 * For documentation see:
 * https://www.apnic.net/about-apnic/corporate-documents/documents/resource-guidelines/rir-statistics-exchange-format/
 */
public class RSEFReader
{
    /** The Scannser from which we will read RSEF entries. */
    private final Scanner reader;

    /** The underlying InputStream that we read from. */
    private final InputStream stream;

    /**
     * Constructs a RSEFReader that will read from the InputStream
     * @param stream The InputStream that will be used to read RSEF entries.
     */
    public RSEFReader(InputStream stream)
    {
        this.stream = stream;
        this.reader = new Scanner(this.stream);
    }

    /**
     * Read the next RSEFObject.
     * @return Returns the next RSEFVersion, RSEFSummary or RSEFRecord.
     */
    public RSEFObject read() throws IOException
    {
        String line = reader.nextLine();
        if (line == null)
            throw new IOException("End of File reached.");

        // Skip the comments.
        while (line.startsWith("#") || line.trim().isEmpty())
            line = reader.nextLine();

        // Divide the line into fields.
        String[] fields = line.split(Pattern.quote("|"));

        // Check if a Version Header is present.
        if (isVersion(fields))
        {
            RSEFVersion version = new RSEFVersion();
            version.version = Double.parseDouble(fields[0]);
            version.registry = fields[1];
            version.serial = fields[2];
            version.records = Integer.parseInt(fields[3]);
            version.startdate = fields[4];
            version.enddate = fields[5];
            version.utcoffset = fields[6];
            return version;
        }

        // Check if a Summary Header is present.
        if (isSummary(fields))
        {
            RSEFSummary summary = new RSEFSummary();
            summary.registry = fields[0];
            summary.type = fields[2];
            summary.count = Integer.parseInt(fields[4]);

            return summary;
        }

        // Parse an RSEFObject
        RSEFRecord record = new RSEFRecord();
        record.registry = fields[0];
        record.organization = fields[1];
        record.type = fields[2];
        record.start = fields[3];
        record.value = Integer.parseInt(fields[4]);
        record.date = fields[5];
        record.status = fields[6];

        // If an extended field is present, parse the opaque-id.
        if(fields.length == 8)
            record.id = fields[7];

        return record;
    }

    /**
     * Checks if a Version Field is present.
     * @param fields The fields present in this line.
     * @return True if a Version Field was present
     */
    private boolean isVersion(String[] fields)
    {
        assert(fields.length >= 5);
        return fields[0].matches("^\\d+\\.?\\d*$");
    }

    /**
     * Checks if a Summary Field is present.
     * @param fields The fields present in this line.
     * @return True if a Summary Field was present.
     */
    private boolean isSummary(String[] fields)
    {
        assert(fields.length >= 5);
        return fields[5].equals("summary");
    }

    /**
     * Check if the end of the input stream has been reached.
     * @return True if the end of the input stream has been reached.
     */
    public boolean isEOF()
    {
        return !reader.hasNextLine();
    }

    /**
     * Closes the Reader and the underlying InputStream.
     */
    public void close() throws IOException
    {
        stream.close();
    }
}
