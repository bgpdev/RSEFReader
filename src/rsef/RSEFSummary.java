package rsef;

/**
 * Instances of the RSEFSummary class represents an RSEF summary line.
 */
public class RSEFSummary extends RSEFObject
{
    /** The registry that this record belongs to. */
    public String registry;

    /** Type of Internet number resource represented in this record. Either asn, ipv4 or ipv6 */
    public String type;

    /** Sum of the number of record lines of this type in the file. */
    public int count;

    @Override
    public void print()
    {
        System.out.println("RIR Specification Exchange Format Summary Header");
        System.out.println("Registry: " + registry);
        System.out.println("Type: " + type);
        System.out.println("Record Count: " + count);
    }
}
