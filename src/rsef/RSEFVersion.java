package rsef;

/**
 * Instances of the RSEFVersion class represents an RSEF version line.
 */
public class RSEFVersion extends RSEFObject
{
    /** The version of the RIR Statistics Exchange Format. */
    public double version;

    /** The registry that this record belongs to. */
    public String registry;

    /** Serial number of this file (within the creating RIR series). */
    public String serial;

    /** Number of records in file, excluding blank lines, summary lines, the version line and comments. */
    public int records;

    /** Start date of time period, in yyyymmdd format. */
    public String startdate;

    /** End date of period, in yyyymmdd format. */
    public String enddate;

    /** Offset from UTC (+/- hours) of local RIR producing file. */
    public String utcoffset;

    @Override
    public void print()
    {
        System.out.println("RIR Specification Exchange Format Version Header");
        System.out.println("Version: " + version);
        System.out.println("Registry: " + registry);
        System.out.println("Serial: " + serial);
        System.out.println("Total Records: " + records);
        System.out.println("Start Date: " + startdate);
        System.out.println("End Date: " + enddate);
        System.out.println("UTF Offset: " + utcoffset);
    }
}
